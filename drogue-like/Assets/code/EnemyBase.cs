﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : MonoBehaviour {

    public float Life;
    public float Velocity;
    public float Damage;

    // Use this for initialization
    void Start ()
    {
        playerStats = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<StatusManager>();
        IsPlayerReach = false;
    }

    protected StatusManager playerStats;
    protected bool IsPlayerReach;
}
