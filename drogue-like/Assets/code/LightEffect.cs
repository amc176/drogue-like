﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightEffect : MonoBehaviour {

    public float LifeTimeEffect
    {
        set { lifeTimeEffect = value; }
    }

    public float ChangeTime;

    // Use this for initialization
    void Start ()
    {
        timer = 0;
        lights = GetComponentsInChildren<Light>();
	}

    // Update is called once per frame
    void Update()
    {
        lifeTimeEffect -= Time.deltaTime;

        if (lifeTimeEffect > 0)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                for (int i = 0; i < lights.Length; ++i)
                {
                    lights[i].color = Random.ColorHSV(0, 1, 1, 1, 1, 1, 1, 1);
                }
                timer = ChangeTime;

                transform.Rotate(new Vector3(0, Random.Range(0, 360), 0));
            }
        }
        else
        {
            gameObject.SetActive(false);
        }
    }


    public float lifeTimeEffect;
    private Light[] lights;
    private float timer;

}
