﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pickups : MonoBehaviour {

	void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Use(collision.gameObject);
        }
    }

    public abstract void Use(GameObject player);
}
