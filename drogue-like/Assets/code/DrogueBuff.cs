﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class DrogueBuff : Pickups
{
    //Increase of these capacities
    public float VelocityPJ = 0;
    public float VelocityGame = 0;
    public float Size = 0;
    public float Accuracy = 0;
    public float DelayController = 0;

    public float TimeLife;

    public VisualEffect Effect;

    public override void Use(GameObject player)
    {
         mStatManager = player.GetComponent<StatusManager>();
        mStatManager.ApplyBuff(this);
        GetComponent<Collider>().enabled = false;
        GetComponent<MeshRenderer>().enabled = false;

        StartCoroutine(RemoveBuffAfterLifeTime());


    }

    IEnumerator RemoveBuffAfterLifeTime()
    {
        yield return new WaitForSeconds(TimeLife);

        mStatManager.RemoveBuff(this);

        Destroy(gameObject);
    }

    private StatusManager mStatManager;

}
