﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    

	[SerializeField]
	private bool automatic;
	[SerializeField]
	private float maxFireSpeed; // shots/second
	[SerializeField]
	private float bulletSpeed;
	[SerializeField]
	private float accuracy; // max degrees
	[SerializeField]
	private Projectile bulletType;

	private Transform childSpriteTransform;
	private float timeSinceLastShot;

	public float Accuracy { set { accuracy = value; } }
	public bool Automatic { get { return automatic; } }

	void Awake() {
		childSpriteTransform = gameObject.transform.Find ("sprite");
		timeSinceLastShot = (1f / maxFireSpeed);
	}

	void Start () {}
	
	// Update is called once per frame
	void Update () {
		childSpriteTransform.Rotate(Vector3.up * 100 * Time.deltaTime);
	}

	void FixedUpdate() {
		timeSinceLastShot += Time.deltaTime;
	}

	public void TryShoot(Vector3 targetPoint, float accuracyModifier = 0) {
		if (!CanShoot ()) return;

		Vector2 direction = GetShotDirection (ref targetPoint, accuracyModifier);
		Vector3 force = new Vector3 (direction.x * bulletSpeed, 0, direction.y * bulletSpeed);
		Projectile proj = Instantiate(bulletType);
		proj.transform.position = gameObject.transform.parent.position + force.normalized * 0.5f;
		proj.GetComponent<Rigidbody> ().AddForce (force, ForceMode.VelocityChange);
		timeSinceLastShot = 0;
	}

	public bool CanShoot() {
		return timeSinceLastShot >= (1 / maxFireSpeed);
	}
		
	private Vector2 GetShotDirection(ref Vector3 targetPoint, float accuracyMod) {
		float degrees = Random.Range (-1f, 1f) * (accuracy + (accuracy * accuracyMod));
		Vector3 dir = Quaternion.Euler (0, degrees, 0) * (targetPoint - transform.position);
		Vector2 dir2d = new Vector2 (dir.x, dir.z);
		dir2d.Normalize ();
		return dir2d;
	}
}
