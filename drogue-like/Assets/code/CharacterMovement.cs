﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour {


    public float Speed
    {
        set { mSpeed = value; }
    }

    public float DelayController
    {
        set { mDelayController = value; }
    }

	private float mSpeed;
    private float mDelayController;


	void Start () 
	{
		mSpeed = 5f;
	}
	
	void FixedUpdate () 
	{
		HandleInput ();
	}

	private void HandleInput()
	{
		float x = Input.GetAxis ("Horizontal");
		float y = Input.GetAxis ("Vertical");

        if(mDelayController > 0)
        {
            StartCoroutine(MoveWithDelay(x, y));
        }
        else
        {
            Vector3 direction = new Vector3(x , 0f, y);
            direction.Normalize();
            transform.position = transform.position + (direction * mSpeed * Time.deltaTime);
        }
		
	}

    IEnumerator MoveWithDelay(float x, float y)
    {
        float time = mDelayController;
        yield return new WaitForSeconds(time);

        if(mDelayController > 0)
        {
            transform.position = transform.position + new Vector3(x * mSpeed * Time.deltaTime, 0f, y * mSpeed * Time.deltaTime);
        }
    }
}
