﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum VisualEffect
{
    NOEffect,
    ZoomInOut,
    Lights
}

public class StatusManager : MonoBehaviour {

    [SerializeField]
    private float Life;

    // X Min    Y Max
    public Vector2 MinMaxVelocityPJ;
    public Vector2 MinMaxVelocityGame;
    public Vector2 MinMaxSize;
    public Vector2 MinMaxAccuracy;
    public Vector2 MinMaxDelayController;

    public float BaseVelocityPJ;
    public float BaseVelocityGame;
    public float BaseAccuracy;
    public float BaseDelayController;


    private float CurrentVelocityPJ;
    private float CurrentVelocityGame;
    private float CurrentSize;
    private float CurrentAccuracy;
    private float CurrentDelayController;

    // Var for camera effects
    public float ZoomInOutIncreaseValue;
    public Vector2 MinMaxZoomValue;

    // Use this for initialization
    void Start ()
    {
        baseSize = gameObject.transform.localScale;
        movement = GetComponent<CharacterMovement>();
        defaultCameraSize = Camera.main.orthographicSize;
        currentCameraSize = defaultCameraSize;
        Reset();

        UpdateBuff();
    }


    void Update()
    {
        UpdateZoomEffectIfEnabled();
    }


    public void ApplyBuff(DrogueBuff Buff)
    {
        CurrentVelocityPJ += Buff.VelocityPJ;
        CurrentVelocityGame += Buff.VelocityGame;
        CurrentSize += Buff.Size;
        CurrentAccuracy += Buff.Accuracy;
        CurrentDelayController += Buff.DelayController;

        UpdateBuff();

        if(Buff.Effect == VisualEffect.ZoomInOut)
        {
            zoomEffectLifeTime += Buff.TimeLife;
        }

        if(Buff.Effect == VisualEffect.Lights)
        {
            lightEffect.lifeTimeEffect = Buff.TimeLife;
            lightEffect.gameObject.SetActive(true);
        }
    }

    public void RemoveBuff(DrogueBuff Buff)
    {
        CurrentVelocityPJ -= Buff.VelocityPJ;
        CurrentVelocityGame -= Buff.VelocityGame;
        CurrentSize -= Buff.Size;
        CurrentAccuracy -= Buff.Accuracy;
        CurrentDelayController -= Buff.DelayController;
        UpdateBuff();
    }

    public void ReceiveDamage(float damage)
    {
        Life -= damage;
    }


    private void UpdateBuff()
    {
        movement.Speed = Mathf.Clamp(CurrentVelocityPJ, MinMaxVelocityPJ.x, MinMaxVelocityPJ.y);
        Time.timeScale = Mathf.Clamp(CurrentVelocityGame, MinMaxVelocityGame.x, MinMaxVelocityGame.y);
        Vector3 newScale = baseSize;
        float newSize = Mathf.Clamp(CurrentSize, MinMaxSize.x, MinMaxSize.y);
        newScale *= newSize;
        transform.localScale = newScale;

        // TODO
        // GEt Weapon , accuracy = Mathf.Clamp(CurrentAccuracy, MinMaxAccuracy.x, MinMaxAccuracy.y);;

        movement.DelayController = Mathf.Clamp(CurrentDelayController, MinMaxDelayController.x, MinMaxDelayController.y);


    }


    private void Reset()
    {
        CurrentVelocityPJ = BaseVelocityPJ;
        CurrentVelocityGame = BaseVelocityGame;
        CurrentSize = 1;
        CurrentAccuracy = BaseAccuracy;
        CurrentDelayController = BaseDelayController;

        zoomEffectLifeTime = 0;

    }

    private void UpdateZoomEffectIfEnabled()
    {
        if (zoomEffectLifeTime > 0)
        {
            zoomEffectLifeTime -= Time.deltaTime;

            if (zoomEffectLifeTime > 0)
            {
                currentCameraSize += (ZoomInOutIncreaseValue * Time.deltaTime);

                if (ZoomInOutIncreaseValue > 0 && currentCameraSize > MinMaxZoomValue.y || ZoomInOutIncreaseValue < 0 && currentCameraSize < MinMaxZoomValue.x)
                {
                    ZoomInOutIncreaseValue = -ZoomInOutIncreaseValue;
                    currentCameraSize = Mathf.Clamp(currentCameraSize, MinMaxZoomValue.x, MinMaxZoomValue.y);
                }
            }
            else
            {
                zoomEffectLifeTime = 0;
                currentCameraSize = defaultCameraSize;
            }
            Camera.main.orthographicSize = currentCameraSize;
        }
    }


    private Vector3 baseSize;
    private CharacterMovement movement;
    private float defaultCameraSize;
    private float currentCameraSize;
    private float zoomEffectLifeTime;

    [SerializeField]
    private LightEffect lightEffect;
}
