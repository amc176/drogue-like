﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWalker : EnemyBase
{
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (IsPlayerReach)
        {
            playerStats.ReceiveDamage(Damage * Time.deltaTime);
        }
        else
        {
            Move();
        }

    }

    private void Move()
    {
        Vector3 direction = playerStats.gameObject.transform.localPosition - transform.localPosition;
        direction.Normalize();
        transform.position = transform.position + (direction * Velocity * Time.deltaTime);
    }



    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            IsPlayerReach = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            IsPlayerReach = false;
        }
    }


}
