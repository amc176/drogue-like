﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponHolder : MonoBehaviour {

	[SerializeField]
	private Weapon equippedWeapon;

	private Weapon weaponInTouch = null;

	private int floorLayerMask;

	void Awake() {
		floorLayerMask = LayerMask.NameToLayer("floor");
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (equippedWeapon && Input.GetMouseButton(0)) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, Mathf.Infinity, (1<<floorLayerMask))) {
				TryShoot (hit.point, 0);
			}
		}

		if (Input.GetMouseButtonDown (1)) {
			if (weaponInTouch) {
				Debug.Log ("Gonna pick up");
				PickUp (weaponInTouch);
			}
			else if (equippedWeapon) {
				Debug.Log ("Gonna drop");
				DropEquippedWeapon ();
			}
		}
	}

	void TryShoot(Vector3 target, float accuracyMod) {
		if (equippedWeapon && equippedWeapon.CanShoot() && 
			(equippedWeapon.Automatic || Input.GetMouseButtonDown(0))) {
			equippedWeapon.TryShoot(target, 0);
		}
	}

	void PickUp(Weapon weapon) {
		Debug.Log ("Pickung up");
		if (equippedWeapon) {
			DropEquippedWeapon ();
		}
		weapon.transform.parent = this.transform;
		weapon.GetComponentInChildren<SpriteRenderer> ().enabled = false;
		weapon.GetComponentInChildren<SphereCollider> ().enabled = false;
		equippedWeapon = weapon;
		weaponInTouch = null;
	}

	void DropEquippedWeapon() {
		if (equippedWeapon) {
			Debug.Log ("Dropping");
			equippedWeapon.GetComponentInChildren<SpriteRenderer> ().enabled = true;
			equippedWeapon.GetComponentInChildren<SphereCollider> ().enabled = true;
			equippedWeapon.transform.SetParent (null);
			equippedWeapon = null;
		}
	}

	void OnTriggerEnter(Collider other) {
		Weapon w = other.GetComponent<Weapon> ();
		if (w) {
			weaponInTouch = w;
			//Debug.Log ("Touching weapon");
		}
	}

	void OnTriggerExit(Collider other) {
		Weapon w = other.GetComponent<Weapon> ();
		if (w && weaponInTouch == w) {
			weaponInTouch = null;
			//Debug.Log ("Untouching weapon");
		}
	}
}
